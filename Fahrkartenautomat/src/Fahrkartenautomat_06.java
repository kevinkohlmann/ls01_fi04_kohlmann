﻿import java.util.Scanner;

class Fahrkartenautomat
{
	
	public static double fahrkartenbestellungErfassen() {
		
		Scanner tastatur = new Scanner(System.in);
		
		int wahlKarte;
		int anzahlTickets;
		double preis;
		double zuZahlenderBetrag;
		String[] fahrkarten = new String[10];
		double[] fahrkartenPreis = new double[10];
		
		//Anlegen der Arrays für die Fahrscheine
		//Welche Vorteile hat man durch diesen Schritt?
		//Antwort: müssen Änderungen bei den Fahrkarten erfolgen, 
		// muss lediglich das Array geändert werden anstatt den
		// Quellcode an verschiedenen Stellen zu bearbeiten und 
		// dabei möglicherweise etwas zu übersehen
		
		fahrkarten[0] = "Einzelfahrschein Berlin AB";
		fahrkarten[1] = "Einzelfahrschein Berlin BC";
		fahrkarten[2] = "Einzelfahrschein Berlin ABC";
		fahrkarten[3] = "Kurzstrecke";
		fahrkarten[4] = "Tageskarte Berlin AB";
		fahrkarten[5] = "Tageskarte Berlin BC";
		fahrkarten[6] = "Tageskarte Berlin ABC";
		fahrkarten[7] = "Kleingruppen-Tageskarte Berlin AB";
		fahrkarten[8] = "Kleingruppen-Tageskarte Berlin BC";
		fahrkarten[9] = "Kleingruppen-Tageskarte Berlin ABC";
		
		fahrkartenPreis[0] = 2.90;
		fahrkartenPreis[1] = 3.30;
		fahrkartenPreis[2] = 3.60;
		fahrkartenPreis[3] = 1.90;
		fahrkartenPreis[4] = 8.60;
		fahrkartenPreis[5] = 9.00;
		fahrkartenPreis[6] = 9.60;
		fahrkartenPreis[7] = 23.50;
		fahrkartenPreis[8] = 24.30;
		fahrkartenPreis[9] = 24.90;
		
		System.out.println("Wählen Sie ihre Wunschfahrkarte aus:\n");
		
		// durch eine Schleife werden nun alle möglichen Fahrkarten mit ihren 
		// Preisen ausgegeben
		// so wird das Menü aus den Arrays erstellt und es muss nur an einer Stelle
		// (nämlich in den Arrays) eine Änderung vorgenommen werden
		// um Fahrkarten zu ergänzen oder zu editieren
		
		for (int i=0; i < fahrkarten.length; i++) 
		{
			int wahl = i + 1;
            System.out.println(fahrkarten[i] + " [" + fahrkartenPreis[i] + "0 EUR] ("+ wahl + ")" );
		}
			System.out.print("\n");
			System.out.print("Ihre Wahl:");
			
		// der Rest des Programms läuft wie gehabt:
			
	    wahlKarte = tastatur.nextInt();
		
	    while (wahlKarte > 10 || wahlKarte < 1) {
	    	System.out.println ("Bitte wählen Sie eine gültige Fahrkarte!");
	    	System.out.println("Ihre Wahl:");
	    	wahlKarte = tastatur.nextInt();
	    }
	    	
	    	preis = fahrkartenPreis[wahlKarte - 1]; 
	    
	    System.out.print("Anzahl Tickets (EURO): ");
	    anzahlTickets = tastatur.nextInt();

	        zuZahlenderBetrag = preis * anzahlTickets;
	    
	    return zuZahlenderBetrag;
	
	}
	
	public static double fahrkartenBezahlen (double zuZahlenderBetrag) {
		
		Scanner tastatur = new Scanner(System.in);
		
		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMünze;
		double rückgabebetrag;
	      
		while(eingezahlterGesamtbetrag < zuZahlenderBetrag) //so lange der eingezahlte Gesamtbetrag kleiner ist als der zu zahlende Betrag (die Variablen werden verglichen) wird die folgende Schleife ausgeführt
	       {
	    	   System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag)); //hier wird der eingezahlte Gesamtbetrag vom zu zahlenden Betrag abgezogen (Subtraktion)
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   eingeworfeneMünze = tastatur.nextDouble(); //hier wird die Variabl eingeworfeneMünze definiert mit der Zahl, die über die Tastatur eingegeben wird
	           eingezahlterGesamtbetrag += eingeworfeneMünze; //die Variable eingezahlterGesamtbetrag wird um die Zahl, die in der Variable eingezahlteMünze liegt, erhöht
	       }
		
		rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		
		return rückgabebetrag;
		
	}
	
	public static void fahrkartenAusgeben() 
	{  
		
	       System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	
	}
	
	public static void rueckgeldAusgeben(double rueckgeld) 
	{
		
		double rückgabebetrag;
		rückgabebetrag = rueckgeld;
		
		if(rückgabebetrag > 0.00) //falls der Rückgabebetrag größer ist als 0 wird die folgende Ausgabe angezeigt, ansonsten nicht
	       {
	    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n", rückgabebetrag);
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.00) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.00;
	           }
	           while(rückgabebetrag >= 1.00) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.00;
	           }
	           while(rückgabebetrag >= 0.50) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.50;
	           }
	           while(rückgabebetrag >= 0.20) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.20;
	           }
	           while(rückgabebetrag >= 0.10) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.10;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	           }
	       }

	     
	       
	    }
	
    public static void main(String[] args)
    {
       
       double zuZahlen;
       double rueckgeld;
       
       
       zuZahlen = fahrkartenbestellungErfassen();
       
       rueckgeld = fahrkartenBezahlen(zuZahlen);
       
       fahrkartenAusgeben();
       
       rueckgeldAusgeben(rueckgeld);
       
       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
               "vor Fahrtantritt entwerten zu lassen!\n"+
               "Wir wünschen Ihnen eine gute Fahrt.");
       
    }
}